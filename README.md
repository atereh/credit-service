## Credit service implementation


**The following OOP patterns are expected to be used:** 


* Builder 


* Factory 


* Template method 


* Decorator 


* Chain of responsibilities 


## Details:


The **Bank** has **Accounts** and **Clients**. The client has a first name, last name, address and passport number (first and last name are required, the rest is optional, the client is built by pattern **Builder**). 

There are 3 types of accounts: 

- Current account, 

- Deposit account,

- Credit account. 


Each account belongs to a different Client. 


**Current account** - a regular account with a fixed interest on the balance. 

You can withdraw money at any time, but you can't leave it in the negative. There are no commissions 


**Deposit account** - an account from which you can't withdraw money until its term ends. 

The interest on the balance depends on the original amount. There are no commissions. 


**Credit account** - has a credit limit within which you can go to the minus of your budget (plus is also possible). 

There is no interest on the balance. There is a fixed Commission for use if the client is in the red. 


Accounts are created using a **Factory** that knows the amount of interest and Commission. 


The factory returns objects only of the General Account type, and the real type is either set explicitly or indirectly (the credit limit can only be for a Credit account, the term can only be for a Deposit, and so on) 


The General Account interface provides an **interface for withdrawing, depositing, and transferring money** (you can only transfer money between accounts of one client). To implement these methods we suggest using the Template method pattern 


If the client's address or passport number is not specified when creating an account, we declare such an account (of any type) as **doubtful**, and prohibit withdrawals and transfers above a certain amount. To implement this functionality we suggest using the **Decorator pattern**


Also, the accounts may receive requests – for example, a request to pay interest or a request to deduct Commission (there is no need for any complex logic – if a request is received, we simply charge the percentage or Commission specified in the invoice, no checks on whether something has already been charged/withdrawn are necessary). 

To process requests we suggest using the **Chain of responsibilities pattern** 


*All these requirements can be treated flexibly. You are required to apply design patterns and protect your solution. Please note that this whole model is very conditional and, for example, does not require any security checks, roles, etc.*


### Contributors:
Alina Terekhova


