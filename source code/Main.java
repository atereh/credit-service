package source;

import source.bank.Bank;
import source.bank.Client;
import source.bank.bank_accounts.IBankAccount;
import source.bank.exceptions.BankOperationException;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Client client = Client.builder("Daniel", "Kunis")
                .withHomeAddress("SPb")
                .withPassport("111")
                .build();
        Bank bank = Bank.getInstance();

        try {
            bank.createAccount(client, new BigDecimal(100000), Bank.AccountTypes.debit);
        } catch (BankOperationException e) {
            System.out.println(e.getLocalizedMessage());
        }
        IBankAccount account = client.getAccounts().get(0);
        System.out.println("Current balance:\t" + account.getBalance());
        try {
            account.withdraw(new BigDecimal(20000));
        } catch (BankOperationException e) {
            System.out.println(e.getLocalizedMessage());
        }
        System.out.println("Current balance:\t" + account.getBalance());

        try {
            bank.accrueDebitInterest(account);
        } catch (BankOperationException e) {
            System.out.println(e.getLocalizedMessage());
        }
        System.out.println("Current balance:\t" + account.getBalance());
    }
}
