package source.bank;

import source.bank.bank_accounts.IBankAccount;
import source.bank.bank_accounts.factories.IBankAccountFactory;
import source.bank.bank_accounts.factories.Implementations.BankAccountFactory;
import source.bank.bank_accounts.implementations.CreditAccount;
import source.bank.bank_accounts.implementations.DebitAccount;
import source.bank.bank_accounts.implementations.DepositAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.request_handlers.RequestHandler;
import source.bank.request_handlers.implementations.AccrueDebitInterestHandler;
import source.bank.request_handlers.implementations.AccrueDepositInterestHandler;
import source.bank.request_handlers.implementations.BankAccountTypeHandler;
import source.bank.request_handlers.implementations.RequestCreditCommissionHandler;

import java.math.BigDecimal;

public class Bank {
    public enum AccountTypes {
        debit,
        credit,
        deposit
    }

    private static Bank ourInstance = new Bank();

    private IBankAccountFactory bankAccountFactory;

    public static Bank getInstance() {
        return ourInstance;
    }

    private Bank() {
        this.bankAccountFactory = new BankAccountFactory();
    }

    public void createAccount(Client client, BigDecimal initialBalance, AccountTypes accountType) throws BankOperationException {
        IBankAccount newBankAccount;
        switch (accountType) {
            case debit:
                newBankAccount = bankAccountFactory.createDebitAccount(client, initialBalance);
                break;
            case credit:
                newBankAccount = bankAccountFactory.createCreditAccount(client, initialBalance);
                break;
            case deposit:
                newBankAccount = bankAccountFactory.createDepositAccount(client, initialBalance);
                break;
            default:
                throw new BankOperationException("Cannot open unsupported bank account type");
        }
        client.addAccount(newBankAccount);
    }

    public void accrueDebitInterest(IBankAccount account) throws BankOperationException {
        RequestHandler validator = new BankAccountTypeHandler(DebitAccount.class);
        RequestHandler handler = new AccrueDebitInterestHandler();
        validator.linkWith(handler);
        validator.handle(account);
    }

    public void accrueDepositInterest(IBankAccount account) throws BankOperationException {
        RequestHandler validator = new BankAccountTypeHandler(DepositAccount.class);
        RequestHandler handler = new AccrueDepositInterestHandler();
        validator.linkWith(handler);
        validator.handle(account);
    }

    public void requestCreditCommission(IBankAccount account) throws BankOperationException {
        RequestHandler validator = new BankAccountTypeHandler(CreditAccount.class);
        RequestHandler handler = new RequestCreditCommissionHandler();
        validator.linkWith(handler);
        validator.handle(account);
    }
}
