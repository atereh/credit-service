package source.bank;

import source.bank.bank_accounts.IBankAccount;

import java.util.ArrayList;

public class Client {
    private String name;
    private String surname;
    private String homeAddress;
    private String passport;
    private ArrayList<IBankAccount> accounts;

    private Client(String name, String surname, String homeAddress, String passport) {
        this.name = name;
        this.surname = surname;
        this.homeAddress = homeAddress;
        this.passport = passport;
        this.accounts = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public String getPassport() {
        return passport;
    }

    public ArrayList<IBankAccount> getAccounts() {
        return accounts;
    }

    public void addAccount(IBankAccount newBankAccount) {
        accounts.add(newBankAccount);
    }

    public static Builder builder(String name, String surname) {
        return new Builder(name, surname);
    }

    public static class Builder {
        private String name;
        private String surname;
        private String homeAddress;
        private String passport;

        private Builder(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public Builder withHomeAddress(String homeAddress) {
            this.homeAddress = homeAddress;
            return this;
        }

        public Builder withPassport(String passport) {
            this.passport = passport;
            return this;
        }

        public Client build() {
            return new Client(name, surname, homeAddress, passport);
        }
    }
}
