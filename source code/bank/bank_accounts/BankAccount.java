package source.bank.bank_accounts;

import source.bank.exceptions.BankOperationException;
import source.bank.Client;

import java.math.BigDecimal;

public abstract class BankAccount implements IBankAccount {
    protected Client cardOwner;
    protected BigDecimal currentBalance;

    public BankAccount(Client cardOwner, BigDecimal currentBalance) {
        this.cardOwner = cardOwner;
        this.currentBalance = currentBalance;
    }

    public void deposit(BigDecimal moneyAmount) throws BankOperationException {
        if (moneyAmount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new BankOperationException("Cannot deposit negative or zero amount of money.");
        }
        currentBalance = currentBalance.add(moneyAmount);
    }

    public void transfer(BigDecimal moneyAmount, BankAccount to) throws BankOperationException {
        this.withdraw(moneyAmount);
        to.deposit(moneyAmount);
    }

    public void withdraw(BigDecimal moneyAmount) throws BankOperationException {
        validateWithDrawOperation(moneyAmount);
        currentBalance = currentBalance.subtract(moneyAmount);
    }

    @Override
    public BigDecimal getBalance() {
        return currentBalance;
    }

    protected abstract void validateWithDrawOperation(BigDecimal moneyAmount) throws BankOperationException;
}
