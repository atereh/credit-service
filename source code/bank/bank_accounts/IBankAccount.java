package source.bank.bank_accounts;

import source.bank.exceptions.BankOperationException;

import java.math.BigDecimal;

public interface IBankAccount {
    void withdraw(BigDecimal moneyAmount) throws BankOperationException;
    void deposit(BigDecimal moneyAmount) throws BankOperationException;
    void transfer(BigDecimal moneyAmount, BankAccount to) throws BankOperationException;
    BigDecimal getBalance();
}
