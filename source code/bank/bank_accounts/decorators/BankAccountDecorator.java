package source.bank.bank_accounts.decorators;

import source.bank.bank_accounts.BankAccount;
import source.bank.bank_accounts.IBankAccount;
import source.bank.exceptions.BankOperationException;

import java.math.BigDecimal;

public class BankAccountDecorator implements IBankAccount {
    private IBankAccount wrapee;

    public BankAccountDecorator(IBankAccount wrapee) {
        this.wrapee = wrapee;
    }

    public void deposit(BigDecimal moneyAmount) throws BankOperationException {
        wrapee.deposit(moneyAmount);
    }

    public void withdraw(BigDecimal moneyAmount) throws BankOperationException {
        wrapee.withdraw(moneyAmount);
    }

    public void transfer(BigDecimal moneyAmount, BankAccount to) throws BankOperationException {
        wrapee.transfer(moneyAmount, to);
    }

    @Override
    public BigDecimal getBalance() {
        return wrapee.getBalance();
    }
}
