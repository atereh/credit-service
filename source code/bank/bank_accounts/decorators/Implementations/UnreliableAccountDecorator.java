package source.bank.bank_accounts.decorators.Implementations;

import source.bank.bank_accounts.BankAccount;
import source.bank.bank_accounts.decorators.BankAccountDecorator;
import source.bank.bank_accounts.IBankAccount;
import source.bank.exceptions.BankOperationException;

import java.math.BigDecimal;

public class UnreliableAccountDecorator extends BankAccountDecorator {
    private static final String errorMessage = "Unreliable clients cannot operate with such amounts of money";

    private BigDecimal operationsMoneyAmountLimit;

    public UnreliableAccountDecorator(IBankAccount wrapee, BigDecimal operationsMoneyAmountLimit) {
        super(wrapee);
        this.operationsMoneyAmountLimit = operationsMoneyAmountLimit;
    }

    @Override
    public void withdraw(BigDecimal moneyAmount) throws BankOperationException {
        if (operationsMoneyAmountLimit.compareTo(moneyAmount) < 0) {
            throw new BankOperationException(errorMessage);
        }
        super.withdraw(moneyAmount);
    }

    @Override
    public void transfer(BigDecimal moneyAmount, BankAccount to) throws BankOperationException {
        if (operationsMoneyAmountLimit.compareTo(moneyAmount) < 0) {
            throw new BankOperationException(errorMessage);
        }
        super.transfer(moneyAmount, to);
    }
}
