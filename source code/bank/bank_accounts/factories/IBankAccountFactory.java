package source.bank.bank_accounts.factories;

import source.bank.bank_accounts.IBankAccount;
import source.bank.Client;

import java.math.BigDecimal;

public interface IBankAccountFactory {
    IBankAccount createCreditAccount(Client client, BigDecimal initialBalance);
    IBankAccount createDebitAccount(Client client, BigDecimal initialBalance);
    IBankAccount createDepositAccount(Client client, BigDecimal initialBalance);
}
