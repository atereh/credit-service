package source.bank.bank_accounts.factories.Implementations;

import source.bank.bank_accounts.decorators.Implementations.UnreliableAccountDecorator;
import source.bank.bank_accounts.factories.IBankAccountFactory;
import source.bank.bank_accounts.IBankAccount;
import source.bank.bank_accounts.implementations.CreditAccount;
import source.bank.bank_accounts.implementations.DebitAccount;
import source.bank.bank_accounts.implementations.DepositAccount;
import source.bank.Client;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

public class BankAccountFactory implements IBankAccountFactory {
    private static final BigDecimal creditLimit = new BigDecimal(-100000);
    private static final BigDecimal limitForUnreliableClients = new BigDecimal(100000);
    private static final BigDecimal commissionFee = new BigDecimal(7000);
    private static final double depositInterestRate = 10;
    private static final double debitInterestRate = 0.1;
    private static final int depositAccountYearsOfServiceLife = 1;

    private boolean isClientUnreliable(Client client) {
        return client.getPassport() == null || client.getHomeAddress() == null;
    }

    @Override
    public IBankAccount createCreditAccount(Client client, BigDecimal initialBalance) {
        IBankAccount bankAccount = new CreditAccount(client, initialBalance, commissionFee, creditLimit);
        if (isClientUnreliable(client)) {
            bankAccount = new UnreliableAccountDecorator(bankAccount, limitForUnreliableClients);
        }
        return bankAccount;
    }

    @Override
    public IBankAccount createDebitAccount(Client client, BigDecimal initialBalance) {
        IBankAccount bankAccount = new DebitAccount(client, initialBalance, debitInterestRate);
        if (isClientUnreliable(client)) {
            bankAccount = new UnreliableAccountDecorator(bankAccount, limitForUnreliableClients);
        }
        return bankAccount;
    }

    @Override
    public IBankAccount createDepositAccount(Client client, BigDecimal initialBalance) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, depositAccountYearsOfServiceLife);
        Date expirationDate = calendar.getTime();

        IBankAccount bankAccount = new DepositAccount(client, initialBalance, expirationDate, depositInterestRate);
        if (isClientUnreliable(client)) {
            bankAccount = bankAccount = new UnreliableAccountDecorator(bankAccount, limitForUnreliableClients);
        }
        return bankAccount;
    }
}
