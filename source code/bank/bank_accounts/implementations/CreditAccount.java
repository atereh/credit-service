package source.bank.bank_accounts.implementations;

import source.bank.bank_accounts.BankAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.Client;

import java.math.BigDecimal;

public class CreditAccount extends BankAccount {
    private BigDecimal creditCommission;
    private BigDecimal creditLimit;

    public CreditAccount(Client cardOwner, BigDecimal currentBalance, BigDecimal creditCommission, BigDecimal creditLimit) {
        super(cardOwner, currentBalance);
        this.creditCommission = creditCommission;
        this.creditLimit = creditLimit;
    }

    public BigDecimal getCreditCommission() {
        return creditCommission;
    }

    @Override
    protected void validateWithDrawOperation(BigDecimal moneyAmount) throws BankOperationException {
        BigDecimal tmpBalance = currentBalance.subtract(moneyAmount);
        if (tmpBalance.compareTo(creditLimit) < 0) {
            throw new BankOperationException("Balance cannot be below credit limit.");
        }
    }
}
