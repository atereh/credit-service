package source.bank.bank_accounts.implementations;

import source.bank.bank_accounts.BankAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.Client;

import java.math.BigDecimal;

public class DebitAccount extends BankAccount {
    private double interestRate;

    public DebitAccount(Client cardOwner, BigDecimal currentBalance, double interestRate) {
        super(cardOwner, currentBalance);
        this.interestRate = interestRate;
    }

    public double getInterestRate() {
        return interestRate;
    }

    @Override
    protected void validateWithDrawOperation(BigDecimal moneyAmount) throws BankOperationException {
        if (currentBalance.compareTo(moneyAmount) < 0) {
            throw new BankOperationException("Cannot withdraw more money than your current balance.");
        }
    }
}
