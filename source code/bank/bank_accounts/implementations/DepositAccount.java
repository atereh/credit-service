package source.bank.bank_accounts.implementations;

import source.bank.bank_accounts.BankAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.Client;

import java.math.BigDecimal;
import java.util.Date;

public class DepositAccount extends BankAccount {
    private Date expirationDate;
    private BigDecimal initialBalance;
    private double interestRate;

    public DepositAccount(Client cardOwner, BigDecimal currentBalance, Date expirationDate, double interestRate) {
        super(cardOwner, currentBalance);
        this.expirationDate = expirationDate;
        this.initialBalance = currentBalance;
        this.interestRate = interestRate;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public double getInterestRate() {
        return interestRate;
    }

    @Override
    protected void validateWithDrawOperation(BigDecimal moneyAmount) throws BankOperationException {
        if (expirationDate.after(new Date()) || currentBalance.compareTo(moneyAmount) < 0) {
            throw new BankOperationException("Cannot withdraw money from active deposit account.");
        }
    }
}
