package source.bank.exceptions;

public class BankOperationException extends Exception {
    private static final String Error = "Bank operation error.";

    public BankOperationException() {
        super(Error);
    }

    public BankOperationException(String message) {
        super(Error + " " + message);
    }
}
