package source.bank.request_handlers;

import source.bank.bank_accounts.IBankAccount;
import source.bank.exceptions.BankOperationException;

public abstract class RequestHandler {
    private RequestHandler next;

    public void linkWith(RequestHandler next) {
        this.next = next;
    }

    protected void callNext(IBankAccount account) throws BankOperationException {
        if (next != null) {
            next.handle(account);
        }
    }

    public abstract void handle(IBankAccount account) throws BankOperationException;
}
