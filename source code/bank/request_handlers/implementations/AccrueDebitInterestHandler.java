package source.bank.request_handlers.implementations;

import source.bank.bank_accounts.IBankAccount;
import source.bank.bank_accounts.implementations.DebitAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.request_handlers.RequestHandler;

import java.math.BigDecimal;

public class AccrueDebitInterestHandler extends RequestHandler {
    @Override
    public void handle(IBankAccount account) throws BankOperationException {
        DebitAccount debitAccount = ((DebitAccount) account);
        BigDecimal amountToDeposit = debitAccount.getBalance().multiply(new BigDecimal(debitAccount.getInterestRate()));
        debitAccount.deposit(amountToDeposit);
    }
}
