package source.bank.request_handlers.implementations;

import source.bank.bank_accounts.IBankAccount;
import source.bank.bank_accounts.implementations.DepositAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.request_handlers.RequestHandler;

import java.math.BigDecimal;

public class AccrueDepositInterestHandler extends RequestHandler {
    @Override
    public void handle(IBankAccount account) throws BankOperationException {
        DepositAccount depositAccount = ((DepositAccount) account);
        BigDecimal amountToDeposit = depositAccount.getInitialBalance().multiply(new BigDecimal(depositAccount.getInterestRate()));
        depositAccount.deposit(amountToDeposit);
    }
}
