package source.bank.request_handlers.implementations;

import source.bank.bank_accounts.IBankAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.request_handlers.RequestHandler;

public class BankAccountTypeHandler extends RequestHandler {
    private Class clazz;

    public BankAccountTypeHandler(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    public void handle(IBankAccount account) throws BankOperationException {
        if (account.getClass() == clazz) {
            this.callNext(account);
        } else {
            throw new BankOperationException("Requested operation cannot be applied to accounts of this type.");
        }
    }
}
