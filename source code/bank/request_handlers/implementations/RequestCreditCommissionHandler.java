package source.bank.request_handlers.implementations;

import source.bank.bank_accounts.IBankAccount;
import source.bank.bank_accounts.implementations.CreditAccount;
import source.bank.exceptions.BankOperationException;
import source.bank.request_handlers.RequestHandler;

import java.math.BigDecimal;

public class RequestCreditCommissionHandler extends RequestHandler {
    @Override
    public void handle(IBankAccount account) throws BankOperationException {
        if (account.getBalance().compareTo(BigDecimal.ZERO) >= 0) {
            account.withdraw(((CreditAccount) account).getCreditCommission());
//        } else {
//            throw new BankOperationException();
        }
    }
}
